package ru.tsc.kirillov.tm.component;

import ru.tsc.kirillov.tm.api.ICommandController;
import ru.tsc.kirillov.tm.api.ICommandRepository;
import ru.tsc.kirillov.tm.api.ICommandService;
import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;
import ru.tsc.kirillov.tm.controller.CommandController;
import ru.tsc.kirillov.tm.repository.CommandRepository;
import ru.tsc.kirillov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        if (processArgument(args))
            close();

        commandController.showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nВведите команду:");
            final String cmdText = scanner.nextLine();
            processCommand(cmdText);
        }
    }

    private boolean processArgument(final String[] args) {
        if (args == null || args.length == 0)
            return false;

        final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private void processCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty())
            return;
        switch (cmd) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showUnexpectedCommand(cmd);
                break;
        }
    }

    private void processArgument(final String arg) {
        if (arg == null || arg.isEmpty())
            return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showUnexpectedArgument(arg);
                break;
        }
    }

    private void close() {
        System.exit(0);
    }

}
